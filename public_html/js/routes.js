'use strict';
angular.module('drischool.routes', ['ui.router'])
        .config(function ($stateProvider, $urlRouterProvider) {
            //
            // For any unmatched url, redirect to /state1
            $urlRouterProvider.otherwise("/dashboard/home");
            console.log('Routes');
            //
            // Now set up the states
            $stateProvider
                    .state('dashboard', {
                        url: "/dashboard",
                        templateUrl: "partials/dashboard.html",
                        abstract: true
                    })
                    .state('dashboard.home', {
                        url: "/home",
                        templateUrl: "partials/dashboard.home.html"
                    })
                    .state('dashboard.bookings', {
                        url: "/bookings",
                        templateUrl: "partials/dashboard.bookings.html"
                    })
                    .state('dashboard.instructors', {
                        url: "/instructors",
                        templateUrl: "partials/dashboard.instructors.html"
                    })
                    .state('dashboard.clients', {
                        url: "/clients",
                        templateUrl: "partials/dashboard.clients.html"
                    })
                    .state('dashboard.settings', {
                        url: "/settings",
                        templateUrl: "partials/dashboard.settings.html"
                    });

        });